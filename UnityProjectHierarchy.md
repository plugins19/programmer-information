# Hierarchy

## Project
1. Assets
2. Packages

### Assets
1. Assets
2. Plugins
3. Resources
4. Prefabs
5. Scenes
6. Scripts

#### Assets (inside main assets)
1. Animations
2. Fonts
3. Materials
4. Models
5. Shaders
6. Sounds
7. Atlases
8. Tiles
9. TilePalletes

#### Plugins 
- to try to put all plugin's SDK to this folder.

#### Resources
- this folder contains installers, databases as ScriptableObject, others databases needed to SDK, packages.
 
#### Scenes
- this folder contains only Scenes

#### Scripts
1. Core
- this folder contains: utils, extentions, Ui custom components, constants...
2. Db
- this folder contains all database scripts as ScriptableObject with correct menuName and fileName.
3. Editors
4. Some folder for each scene
5. Installers
6. Project - scripts, which we can use in other scenes
7. Ui

### Concrete folder Hierarchy
1. subFolder named "Impls"
    - concrete interface realisations
2. needed interfaces
3. classes or structures, which we will use as data models 

### Class names suffixes
- Vo use for data storage

### Some scene folder
- services
- constants
- handlers
- holders
- view-controllers
- holders?
- factories
